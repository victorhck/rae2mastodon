# RAE2Mastodon

___31/03/2020___
Bot que publica en Mastodon la palabra del día y definición correspondiente.
Desarrollador por https://hispatodon.club/@AmbrosTheGreat

Puedes seguir el bot en esta cuenta: https://botsin.space/@RAE_no_oficial

Si se cita el bot responde con la definición de la primera palabra a continuación de su nombre.

Necesariamente precisa de un fichero de configuración para ser lanzado. Un ejemplo es rae_config.ini que se podría lanzar así: python3 palabra.py -me rae_config.ini

Se suministran dos ejemplos de script en bash para este bot.

Soy completamente nuevo en la programación de bots y en Python. el código para poder replicar cuando citan al bot lo he copiado de https://www.bortzmeyer.org/fediverse-bot.html

___01/04/2020___
He modificado el bot para poder desplegarlo en Heroku.
La configuración de la instancia y el token de la app del bot están almacenados en variables de entorno del servidor por seguridad en lugar de hacerse en un fichero, aunque se mantiene esa funcionalidad.
Para poder lanzar el bot se emplea el script bash 'bot'.
Heroku no admite la librería mastodon como genérica así que la he copiado en la carpeta del bot.
Ahora la función de escuchar también hace la función de enviar la palabra diaria de la RAE.

___02/07/2020___
Corregido el control del tiempo para poder hacer las dos tareas al mismo tiempo: escuchar y la palabra del día --> Se lanzan dos trabajadores independientes en el servidor definidos en el Procfile pero con el mismo script de Python.

___03/04/2020___
Ahora la respuesta incluye también a todos los citados en el mensaje original.
Implementado sistema de comandos; las menciones como método para respuesta del bot podían provocar el caos si dos bots se empiezan a citar entre ellos.

___04/04/2020___
El bot ya sólo responde a solicitudes de humanos

___09/04/2020___
Ahora también tootea los estados de Twitter de la RAE (vía nitter.net)

___12/07/2020___
El bot lo integré en un megabot con todos mis otros bots para tener solamente un proceso en Heroku pero ahora lo vuelvo a separar.
Mientras ha estado "unido" se le han integrado los tuits de la ASALE y del Profesor Pardino.

___01/08/2020___
Vuelvo a separar el bot. Con 3 cuentas de Heroku puedo mantener todos los bots.
En el Heroku se lanzan los 3 posibles procesos desde el Scheduler.
Los ficheros accesorios (lista de cuentas y última hora de cada actualización de cuenta) se almacenan en Dropbox.
