#!/usr/bin/env python3
#
# palabra_del_dia
# Librería para extraer la palabra del día de la página de la RAE
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
MASTODON_MAX = 500

# LIBRERIAS
from bs4 import BeautifulSoup
import html
from lxml import html
import requests

# FUNCIONES
def _extraer_cad_palabra():
    # Abrir página, buscar palabra del dia y devolver la cadena que la contiene
    bsObj = BeautifulSoup( requests.get( "https://dle.rae.es/'" ).content, 'lxml' )
    cadena = str( bsObj.find( id='wotd' ) )
    return cadena

def _buscar_palabra( cadena ):
    # Buscar palabra en la  pagina
    palabra = cadena[ cadena.find( "href" )+6 : len( cadena )-8 ]
    palabra = palabra[ palabra.find( ">" )+1 : len( palabra ) ]
    return palabra

def _buscar_enlace( cadena ):
    # Buscar enlace a la defionicion en la  pagina
    enlace = cadena[ cadena.find( "href" )+6 : len( cadena )-8 ]
    enlace = enlace[ 0 : enlace.find( ">" )-1 ]
    enlace = "https://dle.rae.es" + enlace
    return enlace

def _extraer_cad_definicion( la_URL ):
    # Abrir página, buscar palabra del dia y devolver la cadena que la contiene
    bsObj = BeautifulSoup( requests.get( la_URL ).content, 'lxml' )
    cadena = str( bsObj.find( id='resultados' ) )
    doc = html.document_fromstring( cadena )
    cadena = doc.text_content()
    cadena = cadena[ 0 : cadena.find( "©" )-len( "Real Academia Española " ) ]
    cadena = cadena.strip()
    return cadena

def palabra_del_dia( cabecera = '' ):
    definicion = _extraer_cad_palabra()
    palabra = _buscar_palabra( definicion )
    enlace = _buscar_enlace( definicion )
    definicion = _extraer_cad_definicion( enlace )

    salida = cabecera + "La palabra del día es....... " + definicion
    enlace = '\n\nEnlace: ' + enlace
    long_max = MASTODON_MAX - len( enlace )
    
    if len( salida ) > long_max:
        cad_union = ' [...]'
        long_max = long_max - len( cad_union )
        salida = salida[ 0 : long_max ] + ' [...]'

    salida = salida + enlace
    return salida
