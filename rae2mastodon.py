#!/usr/bin/env python3
#
# ra2mastodon
# Crossposter de Twitter a Mastodon para la cuenta de la RAE y algunas cuentas relacionadas
# También muestra la palabra del día
#
# Documentación librería Mastodon: https://mastodonpy.readthedocs.io/en/stable/
#

# CONSTANTES
INST_URL = 'https://botsin.space/'  # URL de la instancia para los bots
MAX_TUITS = 10                      # Máximo número de tuits a importar por ejecución
NOM_ORIG = 'origenes.txt'           # Nombre del fichero del que importar las cuentas de Twitter
T_ESPERA = 601                      # Tiempo de espera entre comprobación de eventos (en segundos)

# LIBRERIAS
from argparse import ArgumentParser
from busca_palabra import comandos as procesa_comandos
from mi_dropbox import Mi_dropbox
from mi_ficheros import Mi_ficheros
from mi_masto import Mi_masto
from palabra_del_dia import palabra_del_dia
from social import getTweets, tootTheTweet

# FUNCIONES
def _args():
    #Argumentos
    parser = ArgumentParser( description='Publica en Mastodon la palabra del día de la RAE y los tuits de su cuenta' )
    parser.add_argument( '-a', '--app_token_rae', help = 'Token de la app para RAE', type = str, required = True )
    parser.add_argument( '-b', '--buscar', help = 'Búsqueda en el DRAE', action = 'store_true', required = False )
    parser.add_argument( '-c', '--crosspost', help = 'Crosspost de Twitter', action = 'store_true', required = False )
    parser.add_argument( '-d', '--dbx_token_rae', help='Token de acceso a Dropbox para la app', type=str, required = True )
    parser.add_argument( '-p', '--palabra_dia', help='Cargar en Mastodon la palabra del día', action = 'store_true', required = False )
    return parser.parse_args()

def _importar_origenes( dbx ):
    # Importa la lista de cuentas de Twitter a crosspostear
    fich = Mi_ficheros( NOM_ORIG )
    if dbx.descargar( fich.en_path(), fich.en_raiz() ):
        lineas = fich.lee_lineas()
    else:
        lineas = []
    salida = []
    for linea in lineas:
        linea = 'https://nitter.net/@' + linea
        salida.append( linea )
    return salida

def _crosspostear( masto, dbx ):
    # Tootear los correspondientes tuits para cada cuenta
    lst_origenes = _importar_origenes( dbx )
    for cuenta in lst_origenes:
        nom_tmp = 'timestamp_' + cuenta.split( '@' )[-1] + '.nfo'
        fich = Mi_ficheros( nom_tmp )
        dbx.descargar( fich.en_path(), fich.en_raiz() )

        tuits = getTweets( cuenta, 'tuit2toot', MAX_TUITS )
        if tuits != None:
            for tuit in tuits:
                tootTheTweet( tuit, masto, nom_tmp )
                print( 'rae2mastodon --> Tuitear ' + str( tuit['url'] ) )
            dbx.cargar( fich.en_path(), fich.en_raiz() )

# MAIN
args = _args()
instancia = INST_URL
masto = Mi_masto( instancia, args.app_token_rae )
dbx = Mi_dropbox( args.dbx_token_rae )

if args.palabra_dia:
    # Sacar palabra del día de la web -- Desde Scheduler
    palabra = str( palabra_del_dia() )
    masto.toot_texto( palabra )
    print( 'rae2mastodon --> Palabra del día\n' + palabra )
elif args.buscar:
    # Buscar palabra en el DRAE y mostrar en Mastodon -- Correr en continuo
    notifs = masto.comprueba_notif( T_ESPERA )
    for notif in notifs:
        print( masto.cuerpo_notif( notif ) )
        procesa_comandos( masto, notif )
elif args.crosspost:
    # Traer tuits -- Desde scheduler
    _crosspostear( masto, dbx )
else:
    print( 'No se ha especificado ninguna opcion.' )
